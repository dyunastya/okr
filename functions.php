<?php


// получили массив всех секций с указанием родителей 

function get_tree_sec() 
{   
    global $link;
    $sql = "select id, name , parent_id 
    from sections    
    where 
    suite_id = 31;" ;

    $result = mysqli_query($link, $sql);
    
    if  (mysqli_num_rows($result) > 0)    
    {
        $cats = array();
    //В цикле формируем массив разделов, ключом будет id родительской категории, а также массив разделов, ключом будет id категории
        while($cat =  mysqli_fetch_assoc($result))
        {
            $cats_ID[$cat['id']][] = $cat;
            $cats[$cat['parent_id']][$cat['id']] =  $cat;
        }
    
    } 
    
    return $cats ;
}

// получили массив требований с указанием родительских секций

function get_req() 

{   
    global $link;

    $sql = "select  cases.id 'id', cases.title 'name', sections.id 'parent_id' 
    from sections  
    right join cases  on sections.id= cases.`section_id`
    where 
    sections.suite_id = 31
    order by depth" ;


    $result = mysqli_query($link, $sql);
     
       if  (mysqli_num_rows($result) > 0)    
        {
        $reqs = array();
    //В цикле формируем массив разделов, ключом будет id родительской категории, а также массив разделов, ключом будет id категории
        while ($req =  mysqli_fetch_assoc($result))
         {
            $reqs_ID[$req['id']][] = $req;
            $reqs[$req['parent_id']][$req['id']] =  $req;
         }
    
        } 
     
    return  $reqs;

}

// Получили массив соотношения кейс - требование
function get_cover() 
{   
    global $link;

    $sql = "select three.id 'parent_id',  cover.id 'id', cover.title 'name'
    from cases three 
    right join cases cover on cover.custom_requirement_id = three.id
    where 
    three.suite_id = 31
    order by three.id" ;

    $result = mysqli_query($link, $sql);
    $covers = mysqli_fetch_all($result, MYSQLI_ASSOC);

        return  $covers;
}

// Перестроили общий массив, в удобный вид

function rebuild_array($array)
{
    if (!is_array($array)) {
        return false;
    }
    $tree = array();
    foreach ($array as $value) {
        $childrens = $value;
        $tree[array_shift($value)['parent_id']]= $childrens;

    }
    unset($value);
    return $tree;

}

// Построили дерево требований

function build_tree($cats,$parent_id = 0)
{
    if(is_array($cats) and isset($cats[$parent_id]))
    {
        $tree = '<ul class="tree">';
        foreach($cats[$parent_id] as $cat)
        {   
            $tree .= '<li class="tree-item">'.' № требования - '.$cat['id'].': ' .$cat['name'];
            if (isset( $cat['coverage']))
            {
                $tree .=  '<br>' ; 
                $tree .= 'Покрытие требования - '.$cat['coverage']. '%';
                $tree .=  '</br>' ; 
            }
            else
            {
                $tree .=''; 
            }
            if (isset( $cat['cases']))
            { 
                $tree .= 'Кейсы по требованию: '.$cat['cases'];
                $tree .=  '</br>' ; 
            }
            else
            {
                $tree .=''; 
            }
            $tree .=  build_tree($cats, $cat['id']);
            $tree .= '</li>';
        }
        $tree .= '</ul>';
    }
    else 
    {
    return null;
    }
    unset($value); 
    return $tree;
}

// обход дерева требований и поиск в нем соответствующих кейсов

function cover_check($cover, $reqs)
{       
   // var_dump($cover,$reqs );die();
        if (empty($cover) && empty($reqs) )
        {
            return [];
        }
        foreach ($cover as $value)
        {  
            $parentId = (int) $value['parent_id'];
            findParentByIdForCoverage($reqs, $parentId);
           
        }

    return  $reqs;
    }

    function findParentByIdForCoverage(array &$forSerch, int $parentId) 
    {   
       // var_dump($forSerch);die();
        foreach ($forSerch as &$requirementForOneParent) 
        {
            foreach ($requirementForOneParent as $idRequirement => &$oneRequirement) 
            {
                if ($idRequirement == $parentId) 
                {
                    $oneRequirement['coverage'] = 100;
                } else {
                    if (!isset($oneRequirement['coverage'])) 
                    {
                        $oneRequirement['coverage'] = 0;
                    }
                }
            }
        }
    }

    function AddCasesInArray($cover, $reqs){
        if (empty($cover) && empty($reqs) )
        {
            return [];
        }

        foreach ($cover as $value)
        {  
            $parentId = (int) $value['parent_id'];
            $IdCases = (int) $value['id'];
            $NameCases =  $value['name'];
            findParentByIdForCase($reqs, $parentId, $IdCases, $NameCases);
           
        }

    return  $reqs;
    }

    function findParentByIdForCase(array &$forSerch, int $parentId, int $IdCases, string $NameCases ) 
    {
        foreach ($forSerch as &$requirementForOneParent) 
        {
           
            foreach ($requirementForOneParent as $idRequirement => &$oneRequirement) 
            {
               
                if ($idRequirement == $parentId) 
                {
                    if (isset ($oneRequirement['cases'])) 
                    {
                        $oneRequirement['cases'] .= '; '. 'C'.$IdCases .'. '. $NameCases;
                    }
                else
                    $oneRequirement['cases'] = 'C'.$IdCases .'. '. $NameCases;
                } 
            }
        }
    }

    // function CountTotalCoverage($cover, $reqs){
    //     if (empty($cover) && empty($reqs) )
    //     {
    //         return [];
    //     }

    //     foreach ($cover as $value)
    //     {  
    //         $parentId = (int) $value['parent_id'];
    //         $CountCoverage  = (int) $value['parent_id'];

    //         CountCases($reqs, $parentId, $CountCoverage);
           
    //     }

    // return  $reqs;
    // }

    // function CountCases(array $CountCoverageReqs,array $Sections, int $parentId, int $CountCoverage )
    // {       

    //    foreach ($CountCoverageReqs as &$requirementForOneParent) 
    //    {
    //     $Countcase = 0; 
    //     foreach ($requirementForOneParent as $idRequirement => &$oneRequirement) 
    //     {
           
    //         if ($idRequirement == $parentId) 
    //         {
               
    //            $Countcase++ ;
    //            var_dump($idRequirement);die();
    //         }

    //     }
        
    //    }

    // } 


$sections = get_tree_sec();
$requirement= get_req(); 
$cases = get_cover ();
$coverageInRequirement = cover_check($cases, $requirement);
$casesInRequirement = AddCasesInArray($cases, $coverageInRequirement);

//$CountTotalCoverage= CountTotalCoverage($casesInRequirement, $sections);

$full_array = array_merge($sections,$casesInRequirement);
$rebuild_reqs= rebuild_array($full_array);

echo json_encode($rebuild_reqs);
die();
$tree_req = build_tree ($rebuild_reqs);


//$check = cover_check($cover, $reqs);